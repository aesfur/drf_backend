from split_settings.tools import optional, include

include(
    'settings/base.py',
    'settings/db.py',
    'settings/rest_api.py',
    'settings/email.py',
    'settings/auth.py',
    optional('settings/local.py')
)
