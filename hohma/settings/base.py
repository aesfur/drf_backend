import json
import os
import sentry_sdk

from dotenv import load_dotenv
from sentry_sdk.integrations.django import DjangoIntegration

load_dotenv()


def get_env(variable_name):
    value = os.getenv(variable_name)
    if value is None:
        raise ValueError(f"{variable_name} is not presented in "
                         f"environment variables. Check your .env file")
    if str(value).lower() in ("true", "false"):
        return str(value).lower() == "true"
    return value


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = get_env('DEBUG')
DEBUG_PROPAGATE_EXCEPTIONS = True

SECRET_KEY = get_env('SECRET_KEY')
ALLOWED_HOSTS = json.loads(get_env("ALLOWED_HOSTS"))

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'rest_framework',
    'rest_framework.authtoken',

    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.google',
    'allauth.socialaccount.providers.vk',

    'rest_auth',
    'rest_auth.registration',

    'storages',

    'taggit',
    'taggit_serializer',
    'corsheaders',
    'django_filters',
    'django_cleanup.apps.CleanupConfig',

    'app_list.core.apps.CoreConfig',
    'app_list.authenticate.apps.AuthConfig',
    'app_list.fun.apps.FunConfig'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'hohma.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'hohma.wsgi.application'

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

CORS_ORIGIN_ALLOW_ALL = True

AWS_S3_SECURE_URLS = False
AWS_DEFAULT_ACL = None
AWS_S3_REGION_NAME = get_env('AWS_S3_REGION_NAME')
AWS_ACCESS_KEY_ID = get_env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env('AWS_SECRET_ACCESS_KEY')
AWS_S3_CUSTOM_DOMAIN = 'cdn.hohma.club'
MEDIA_BUCKET_NAME = get_env('MEDIA_BUCKET_NAME')
STATIC_BUCKET_NAME = get_env('STATIC_BUCKET_NAME')
CUSTOM_MEDIA_DOMAIN = AWS_S3_CUSTOM_DOMAIN
CUSTOM_STATIC_DOMAIN = '%s.s3.amazonaws.com' % STATIC_BUCKET_NAME

if not DEBUG:
    DEFAULT_FILE_STORAGE = 'app_list.core.storages.MediaStorage'
    STATICFILES_STORAGE = 'app_list.core.storages.StaticStorage'

IMAGES_TYPES = ['image/png', 'image/jpeg']
VIDEO_TYPES = ['video/mp4']
ANIMATION_TYPES = ['image/gif']
ALLOWED_MEDIA_TYPES = IMAGES_TYPES + VIDEO_TYPES + ANIMATION_TYPES

if not DEBUG:
    sentry_sdk.init(
        dsn=get_env('SENTRY_DSN'),
        integrations=[DjangoIntegration()]
    )
