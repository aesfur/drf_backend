AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend'
)

ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_ADAPTER = 'app_list.authenticate.adapter.AccountAdapter'
SOCIALACCOUNT_ADAPTER = 'app_list.authenticate.adapter.SocialAccountAdapter'
