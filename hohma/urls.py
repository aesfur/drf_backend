from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

from app_list.fun.views.base import NotImplementedView

urlpatterns = [
    path(
        'api/schema-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),

    path('api/', include('app_list.fun.urls')),
    path('api/auth/', include('app_list.authenticate.urls')),

    path('admin/', admin.site.urls)
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
else:
    urlpatterns += [re_path(r'.*', NotImplementedView.as_view())]
