from django.urls import include, path

from rest_auth import views
from rest_auth.registration.views import RegisterView

from app_list.authenticate.views import GoogleLogin, VkLogin

urlpatterns = [
    path('account/', include('allauth.urls')),

    path('registration/', RegisterView.as_view(), name='rest_register'),
    path('login/', views.LoginView.as_view(), name='rest_login'),
    path('logout/', views.LogoutView.as_view(), name='rest_logout'),
    path('me/', views.UserDetailsView.as_view(), name='rest_user_details'),
    path('password/change/', views.PasswordChangeView.as_view(),
         name='rest_password_change'),

    path('google/', GoogleLogin.as_view(), name='google_login'),
    path('vk/', VkLogin.as_view(), name='vk_login'),
]
