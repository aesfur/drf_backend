from rest_framework.authentication import TokenAuthentication


class HohmaAuthentication(TokenAuthentication):
    keyword = 'Bearer'
