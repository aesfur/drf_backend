from django.conf import settings
from django.contrib.auth import get_backends, get_user_model, login
from django.shortcuts import resolve_url

from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.utils import perform_login
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

User = get_user_model()


class SocialAccountAdapter(DefaultSocialAccountAdapter):

    def pre_social_login(self, request, sociallogin):
        user = sociallogin.user
        if user.id:
            return
        try:
            customer = User.objects.get(email=user.email)
            sociallogin.state['process'] = 'connect'
            perform_login(request, customer, 'none')
        except User.DoesNotExist:
            pass


class AccountAdapter(DefaultAccountAdapter):

    def login(self, request, user):
        if not hasattr(user, 'backend'):
            from allauth.account.auth_backends import AuthenticationBackend
            backends = get_backends()
            backend = None
            for b in backends:
                if isinstance(b, AuthenticationBackend):
                    backend = b
                    break
                elif not backend and hasattr(b, 'get_user'):
                    backend = b
            backend_path = '.'.join([backend.__module__,
                                     backend.__class__.__name__])
            user.backend = backend_path
        if getattr(settings, 'REST_SESSION_LOGIN', True):
            login(request, user)

    def get_login_redirect_url(self, request):
        if getattr(settings, 'REST_SESSION_LOGIN', True):
            assert request.user.is_authenticated
            url = settings.LOGIN_REDIRECT_URL
            return resolve_url(url)
