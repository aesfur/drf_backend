from rest_auth.serializers import UserDetailsSerializer
from rest_framework import serializers


class TokenSerializer(serializers.Serializer):
    key = serializers.CharField()
    user = UserDetailsSerializer()
