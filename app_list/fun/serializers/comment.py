from rest_framework import serializers

from app_list.fun.models import Comment
from app_list.fun.serializers import UserDetailSerializer


class CommentDetailSerializer(serializers.ModelSerializer):
    user = UserDetailSerializer()
    post_id = serializers.IntegerField(source='post.id')
    post_uid = serializers.CharField(source='post.uid')

    class Meta:
        model = Comment
        fields = [
            'id',
            'user',
            'post_id',
            'post_uid',
            'text',
            'created_at',
            'modified_at'
        ]


class CommentSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    def to_representation(self, instance):
        return CommentDetailSerializer(instance, context=self.context).data

    def create(self, validated_data):
        validated_data['post'] = self.context.get('post')
        return super().create(validated_data)

    class Meta:
        model = Comment
        fields = ['text', 'user']
