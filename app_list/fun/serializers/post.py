from django.db.transaction import atomic

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from taggit_serializer.serializers import (
    TaggitSerializer, TagListSerializerField
)

from app_list.fun.models import Media, Post
from app_list.fun.serializers import UserDetailSerializer
from app_list.fun.serializers.media import (
    MediaDetailSerializer, MediaSerializer
)


class PostDetailSerializer(TaggitSerializer, serializers.ModelSerializer):
    media = MediaDetailSerializer()
    user = UserDetailSerializer()
    tags = TagListSerializerField(allow_null=True, default=[])
    is_liked = serializers.SerializerMethodField()
    is_disliked = serializers.SerializerMethodField()
    count_like = serializers.ReadOnlyField()
    count_dislike = serializers.ReadOnlyField()
    count_comment = serializers.ReadOnlyField()

    def get_is_liked(self, instance):
        request = self.context.get('request')
        if not request.user.is_anonymous:
            return instance.is_liked(request.user.id)
        return False

    def get_is_disliked(self, instance):
        request = self.context.get('request')
        if not request.user.is_anonymous:
            return instance.is_disliked(request.user.id)
        return False

    class Meta:
        model = Post
        fields = [
            'id',
            'uid',
            'title',
            'description',
            'type_post',
            'user',
            'tags',
            'media',
            'is_liked',
            'is_disliked',
            'count_like',
            'count_dislike',
            'count_comment',
            'created_at',
            'modified_at'
        ]


class PostSerializer(TaggitSerializer, serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    tags = TagListSerializerField(allow_null=True, default=[])
    media = serializers.SlugRelatedField(
        slug_field='uid', queryset=Media.objects
    )

    def validate_media(self, media):
        request = self.context.get('request')
        if not media.user == request.user:
            raise ValidationError('Media file not found')
        if media.in_post:
            raise ValidationError('Post with this media already exists')
        return media

    def to_representation(self, instance):
        serializer = PostDetailSerializer(instance, context=self.context)
        return serializer.data

    class Meta:
        model = Post
        fields = [
            'title',
            'description',
            'media',
            'tags',
            'user'
        ]


class ParsingPostSerializer(serializers.Serializer):
    fake_likes = serializers.IntegerField(default=0)
    fake_dislikes = serializers.IntegerField(default=0)
    description = serializers.CharField(default='')
    title = serializers.CharField(max_length=255, default='')
    source = serializers.CharField(max_length=255, default='')
    external_id = serializers.CharField(max_length=255, default='')
    file = serializers.FileField()
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    @atomic
    def create(self, validated_data):
        file = validated_data.pop('file')
        media_serializer = MediaSerializer(
            data={'file': file}, context=self.context
        )
        media_serializer.is_valid(raise_exception=True)
        media = media_serializer.save()
        return Post.objects.create(**validated_data, media=media)

    def to_representation(self, instance):
        return {'message': 'Post successfully saved', 'uid': instance.uid}
