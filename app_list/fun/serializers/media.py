from django.conf import settings

from magic import from_buffer as get_content_type
from rest_framework import serializers

from app_list.fun.models import Media


class MediaDetailSerializer(serializers.ModelSerializer):
    url = serializers.URLField(source='file.url')
    size = serializers.IntegerField(source='file.size')

    class Meta:
        model = Media
        fields = [
            'uid',
            'type_media',
            'name',
            'extension',
            'dominant_color',
            'url',
            'size'
        ]


class MediaSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    def validate(self, data):
        file = data.get('file')
        content_type = get_content_type(file.read(), mime=True)

        if content_type not in settings.ALLOWED_MEDIA_TYPES:
            raise serializers.ValidationError('Invalid file type')

        file_name = file.name.split('.')[0] if '.' in file.name else file.name
        extension = content_type.split('/')[-1]
        file.name = file_name
        file.content_type = content_type

        data['name'] = file_name
        data['extension'] = extension

        if content_type in settings.IMAGES_TYPES:
            data['type_media'] = Media.PICTURE
        elif content_type in settings.VIDEO_TYPES:
            data['type_media'] = Media.VIDEO
        elif content_type in settings.ANIMATION_TYPES:
            data['type_media'] = Media.ANIMATION

        return data

    def to_representation(self, instance):
        serializer = MediaDetailSerializer(instance, context=self.context)
        return serializer.data

    class Meta:
        model = Media
        fields = ['file', 'user']
