from rest_framework import serializers


class TagListSerializer(serializers.Serializer):
    tags = serializers.ListField(
        child=serializers.CharField()
    )
