import django_filters

from app_list.fun.models import Post


class TagsFilter(django_filters.Filter):
    def filter(self, qs, value):
        if value not in (None, ''):
            tags = [v for v in value.split(',')]
            return qs.filter(tags__name__in=tags).distinct()
        return qs


class PostFilter(django_filters.FilterSet):
    user = django_filters.CharFilter(
        field_name='user__username',
        help_text='Username filter'
    )
    tags = TagsFilter(
        field_name='tags',
        lookup_expr='in',
        help_text='A comma-separated list of tags.'
    )

    class Meta:
        model = Post
        fields = ['user', 'tags']
