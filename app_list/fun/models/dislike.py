from django.contrib.auth import get_user_model
from django.db import models

from app_list.fun.models import DateTimeModel, Post

User = get_user_model()


class Dislike(DateTimeModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'post']
        verbose_name = 'Dislike'
        verbose_name_plural = 'Dislikes'

    def __str__(self):
        return ' '.join([self.user.username, ':', self.post.uid])
