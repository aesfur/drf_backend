import uuid

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from colorthief import ColorThief

from app_list.fun.models import DateTimeModel
from app_list.fun.utils import upload_to

User = get_user_model()


class Media(DateTimeModel):
    PICTURE = 'picture'
    VIDEO = 'video'
    ANIMATION = 'animation'

    TYPES_MEDIA = (
        (PICTURE, 'picture'),
        (VIDEO, 'video'),
        (ANIMATION, 'animation')
    )

    uid = models.CharField(
        max_length=8,
        unique=True,
        editable=False,
        verbose_name='Media uid',
    )
    type_media = models.CharField(max_length=255, choices=TYPES_MEDIA)
    name = models.CharField(max_length=255)
    extension = models.CharField(max_length=255)
    file = models.FileField(upload_to=upload_to)
    dominant_color = models.CharField(max_length=255, blank=True)
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='media',
        null=True
    )

    class Meta:
        verbose_name = 'Media file'
        verbose_name_plural = 'Media files'

    def __str__(self):
        return self.uid

    @property
    def in_post(self):
        return hasattr(self, 'post')

    def save(self, *args, **kwargs):
        if not self.pk:
            while True:
                uid = uuid.uuid4().hex[:8]
                if not Media.objects.filter(uid=uid).first():
                    break
            self.uid = uid
        super().save(*args, **kwargs)


@receiver(post_save, sender=Media)
def calc_dominant_color(sender, instance, created, **kwargs):
    if created and instance.type_media == Media.PICTURE and instance.file:
        color_thief = ColorThief(instance.file)
        dominant_color = color_thief.get_color(quality=1)
        instance.dominant_color = dominant_color
        instance.save()
