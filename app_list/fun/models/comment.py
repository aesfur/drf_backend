from django.contrib.auth import get_user_model
from django.db import models

from app_list.fun.models import DateTimeModel, Post

User = get_user_model()


class Comment(DateTimeModel):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'

    def __str__(self):
        return ''.join([self.text[:50], '..'])
