from django.contrib.auth import get_user_model
from django.db import models

from app_list.fun.models import DateTimeModel, Post

User = get_user_model()


class Like(DateTimeModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'post']
        verbose_name = 'Like'
        verbose_name_plural = 'Likes'

    def __str__(self):
        return ' '.join([self.user.username, ':', self.post.uid])
