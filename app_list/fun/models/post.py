import uuid

from django.contrib.auth import get_user_model
from django.db import models

from taggit.managers import TaggableManager

from app_list.fun.models import DateTimeModel, Media

User = get_user_model()


class Post(DateTimeModel):
    uid = models.CharField(
        max_length=8,
        unique=True,
        editable=False,
        verbose_name='Public identifier',
    )
    title = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    media = models.OneToOneField(Media, on_delete=models.PROTECT)
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='posts',
        null=True
    )
    likes = models.ManyToManyField(
        User,
        through='Like',
        related_name='likes'
    )
    dislikes = models.ManyToManyField(
        User,
        through='Dislike',
        related_name='dislikes'
    )
    comments = models.ManyToManyField(
        User,
        through='Comment',
        related_name='comments'
    )
    fake_likes = models.IntegerField(default=0)
    fake_dislikes = models.IntegerField(default=0)
    source = models.CharField(max_length=255, blank=True)
    external_id = models.CharField(max_length=255, blank=True)

    tags = TaggableManager(blank=True)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title if self.title else self.uid

    def save(self, *args, **kwargs):
        if not self.pk:
            while True:
                uid = uuid.uuid4().hex[:8]
                if not Post.objects.filter(uid=uid).first():
                    break
            self.uid = uid
        super().save(*args, **kwargs)

    @property
    def count_like(self):
        return self.likes.all().count() + self.fake_likes

    @property
    def count_dislike(self):
        return self.dislikes.all().count() + self.fake_dislikes

    @property
    def count_comment(self):
        return self.comments.all().count()

    @property
    def hot_rating(self):
        return self.count_like + self.count_comment - self.count_dislike

    @property
    def type_post(self):
        return self.media.type_media if self.media else None

    def is_liked(self, user_id):
        return bool(self.likes.filter(id=user_id))

    def is_disliked(self, user_id):
        return bool(self.dislikes.filter(id=user_id))
