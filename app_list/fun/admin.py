from django.contrib import admin

from app_list.fun.models import Comment, Dislike, Like, Media, Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['uid', 'title', 'created_at', 'user']
    list_display_links = ['uid', 'title']
    search_fields = ['title']


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['user', 'post', 'created_at']
    list_display_links = ['user', 'post']


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'created_at']
    list_display_links = ['__str__']


@admin.register(Dislike)
class DislikeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'created_at']
    list_display_links = ['__str__']


@admin.register(Media)
class MediaAdmin(admin.ModelAdmin):
    list_display = [
        'uid', 'name', 'type_media', 'extension', 'created_at', 'user'
    ]
    list_display_links = ['uid', 'name']
    search_fields = ['uid', 'name']
    list_filter = ['type_media']
