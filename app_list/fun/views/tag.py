from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from taggit.models import Tag

from app_list.fun.serializers import TagListSerializer


class TagView(ListAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagListSerializer
    pagination_class = None

    def get(self, request, **kwargs):
        queryset = self.get_queryset().values_list('name', flat=True)
        serializer = self.get_serializer({'tags': queryset})
        return Response(serializer.data)
