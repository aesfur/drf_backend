from django.shortcuts import get_object_or_404

from rest_framework import status, views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from app_list.fun.models import Dislike, Like, Post


class LikeView(views.APIView):
    permission_classes = [IsAuthenticated]
    model = Like

    def get_post(self):
        return get_object_or_404(Post, uid=self.kwargs['post_uid'])

    def put(self, request, *args, **kwargs):
        post = self.get_post()
        user = request.user
        Dislike.objects.filter(user=user, post=post).delete()
        Like.objects.filter(user=user, post=post).delete()
        self.model.objects.update_or_create(user=user, post=post)
        return Response(status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        post = self.get_post()
        self.model.objects.filter(user=request.user, post=post).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DislikeView(LikeView):
    model = Dislike
