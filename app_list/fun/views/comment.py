from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from app_list.authenticate.permissions import IsOwnObjectOrReadOnly
from app_list.fun.models import Comment, Post
from app_list.fun.serializers import CommentSerializer


class CommentViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnObjectOrReadOnly]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    pagination_class = LimitOffsetPagination
    pagination_class.default_limit = 25
    lookup_url_kwarg = 'comment_id'

    def get_post(self):
        return get_object_or_404(Post, uid=self.kwargs['post_uid'])

    def get_queryset(self):
        queryset = super().get_queryset()
        post = self.get_post()
        return queryset.filter(post=post)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        post = self.get_post()
        context.update({'post': post})
        return context
