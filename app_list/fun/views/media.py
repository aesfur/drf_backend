from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from app_list.fun.serializers import MediaSerializer


class MediaView(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = MediaSerializer
