from datetime import timedelta

from django.utils import timezone

from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination

from app_list.fun.filters import PostFilter
from app_list.fun.models import Post
from app_list.fun.serializers import PostDetailSerializer


class FeedView(ListAPIView):
    queryset = Post.objects.all().select_related('user').prefetch_related(
        'likes', 'dislikes')
    serializer_class = PostDetailSerializer
    pagination_class = LimitOffsetPagination
    pagination_class.default_limit = 25

    def sort_q(self, q):
        return sorted(q, key=lambda p: p.hot_rating, reverse=True)

    def get_queryset(self):
        q = super().get_queryset()
        q = PostFilter(self.request.query_params, queryset=q).qs
        feed_type = self.kwargs.get('feed_type').lower()
        one_week_ago = timezone.now() - timedelta(days=7)
        if feed_type == 'hot':
            hot_q = q.filter(created_at__gte=one_week_ago)
            old_q = q.exclude(id__in=hot_q)
            return self.sort_q(hot_q) + self.sort_q(old_q)
        elif feed_type == 'new':
            return q.order_by('-created_at')
        return q
