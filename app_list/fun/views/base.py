from rest_framework import views
from rest_framework.exceptions import NotFound


class NotImplementedView(views.APIView):
    error_message = ('Page not found. Either you forgot to add slash '
                     'at the end of url or address does not exist.')
    swagger_schema = None

    def get(self, request):
        raise NotFound(detail=self.error_message, code=404)

    def post(self, request):
        return self.get(request)

    def patch(self, request):
        return self.get(request)

    def put(self, request):
        return self.get(request)

    def delete(self, request):
        return self.get(request)
