from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, viewsets
from rest_framework.filters import SearchFilter
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAdminUser, IsAuthenticatedOrReadOnly

from app_list.authenticate.permissions import IsOwnObjectOrReadOnly
from app_list.fun.filters import PostFilter
from app_list.fun.models import Post
from app_list.fun.serializers import PostSerializer
from app_list.fun.serializers.post import ParsingPostSerializer


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnObjectOrReadOnly]
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filter_class = PostFilter
    serializer_class = PostSerializer
    pagination_class = LimitOffsetPagination
    pagination_class.default_limit = 25
    lookup_field = 'uid'
    lookup_url_kwarg = 'post_uid'
    search_fields = ['title']

    def get_queryset(self):
        return Post.objects.all().select_related(
            'user'
        ).prefetch_related('likes', 'dislikes')


class ParsingPostView(generics.CreateAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = ParsingPostSerializer
