from django.apps import AppConfig


class FunConfig(AppConfig):
    name = 'app_list.fun'
