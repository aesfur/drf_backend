from django.urls import include, path

from app_list.fun import views

list_create = {
    'get': 'list',
    'post': 'create'
}

retrieve_update_destroy = {
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
}

comment_urls = [
    path('', views.CommentViewSet.as_view(list_create),
         name='list_create_comments'),
    path('<comment_id>/',
         views.CommentViewSet.as_view(retrieve_update_destroy),
         name='retrieve_update_comment')
]

post_detail_urls = [
    path('', views.PostViewSet.as_view(retrieve_update_destroy),
         name='retrieve_update_post'),
    path('like/', views.LikeView.as_view(), name='like'),
    path('dislike/', views.DislikeView.as_view(), name='dislike'),
    path('comments/', include(comment_urls))
]

post_urls = [
    path('', views.PostViewSet.as_view(list_create), name='list_create_post'),
    path('<post_uid>/', include(post_detail_urls))
]

urlpatterns = [
    path('fun/', include(post_urls)),
    path('media/', views.MediaView.as_view(), name='create_media'),
    path('users/<username>/', views.UserViewSet.as_view(),
         name='retrieve_user'),
    path('feed/<feed_type>/', views.FeedView.as_view(), name='feed'),
    path('tags/', views.TagView.as_view(), name='tags'),
    path('backroom/parser/v1/upload-post/', views.ParsingPostView.as_view(),
         name='backroom_upload')
]
