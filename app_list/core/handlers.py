from rest_framework.views import exception_handler


def hohma_exception_handler(exc, context):
    response = exception_handler(exc, context)

    unified_response = {}
    errors = []

    if response is not None:
        message = response.data.get('detail') or response.data.get('message')
        if hasattr(exc, 'detail') and hasattr(exc.detail, 'items'):
            unified_response['message'] = 'Validation failed'
            for key, value in exc.detail.items():
                error = {'message': ' '.join(value)}
                if not key == 'non_field_errors':
                    error.update({'field': key})
                errors.append(error)
            unified_response['errors'] = errors
        else:
            unified_response['message'] = message

        response.data = unified_response

    return response
