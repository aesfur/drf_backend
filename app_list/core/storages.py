from django.conf import settings

from storages.backends.s3boto3 import S3Boto3Storage


class MediaStorage(S3Boto3Storage):
    file_overwrite = False
    custom_domain = settings.CUSTOM_MEDIA_DOMAIN
    bucket_name = settings.MEDIA_BUCKET_NAME


class StaticStorage(S3Boto3Storage):
    file_overwrite = True
    custom_domain = settings.CUSTOM_STATIC_DOMAIN
    bucket_name = settings.STATIC_BUCKET_NAME
