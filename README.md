# Hohma project

## Local deploy instructions

1) Create and activate virtulalenv:
- `python3 -m venv .venv`  
- `source .venv/bin/activate`  

2) pip install -r requirements.txt

3) Create postgresql db

4) Create .env file in hohma directory from template .env.template.py and add your local configuration to it.

5) python manage.py migrate

6) python manage.py runserver

7) Перед пушем нужно выполнить команду flake8, чтобы проверить форматирование кода

При DEBUG=True, медиа сохраняются локально.
AWS_ACCESS_KEY_ID и AWS_SECRET_ACCESS_KEY указывать не обязательно.